const mainChoise = document.getElementById('main-choise');
const subMenu = document.querySelector('.sub-menu');
const mainChoiseImg = document.getElementById('main-choise-img');
const list = document.querySelector('.sub-menu ul');


mainChoise.addEventListener('click', function(e){

	if(!e.target.nextElementSibling.classList.contains('sub-menu--visible')){
		
		e.target.nextElementSibling.classList.add('sub-menu--visible');
		mainChoise.style.borderColor = '#00A0F2';
		mainChoise.style.borderBottom ='0';
		mainChoiseImg.style.transform = "rotate(+180deg)";
	
 
	}
	else{
		e.target.nextElementSibling.classList.remove('sub-menu--visible');
		mainChoise.style.borderColor = '#dcdcdc';
		mainChoise.style.borderBottom = '1px';
		mainChoiseImg.style.transform = "rotate(0deg)";
	}
})


list.addEventListener('click',function (e) {
	console.log(e.target.dataset)
	document.querySelector('[id="main-choise-text"]').innerHTML = e.target.dataset.value;
	subMenu.classList.remove('sub-menu--visible');
	mainChoise.style.borderColor = '#dcdcdc';
	mainChoise.style.borderBottom = '1px';
	mainChoiseImg.style.transform = "rotate(0deg)";
})